import 'package:festivalse/model/artist.dart';
import 'package:festivalse/service/artist.service.dart';
import 'package:festivalse/view_model/user.view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class ArtistViewModel extends ChangeNotifier {
  List<Artist> artists = [];

  void getArtists() async {
    artists = (await ArtistService.getArtists())!;
    print(artists);
    notifyListeners();
  }

  void addArtist(BuildContext context, Artist artist) async {
    String? res = await ArtistService.addArtist(artist.name, artist.passageStart, artist.passageEnd);
    if (res != "Unauthorized") {
      artists.add(artist);
      notifyListeners();
    } else {
      Provider.of<UserViewModel>(context, listen: false).logout();
    }
    Navigator.pop(context);
  }

  void removeArtist(BuildContext context, Artist artist) async {
    String? res = await ArtistService.removeArtist(artist.id);
    if (res != "Unauthorized") {
      artists.remove(artist);
      notifyListeners();
    } else {
      Provider.of<UserViewModel>(context, listen: false).logout();
    }
    Navigator.pop(context);
  }
}
