import 'package:festivalse/view_model/artist.view_model.dart';
import 'package:festivalse/view_model/user.view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class ArtistsPage extends StatefulWidget {
  const ArtistsPage({Key? key}) : super(key: key);

  @override
  State<ArtistsPage> createState() => _ArtistsPageState();
}

class _ArtistsPageState extends State<ArtistsPage> {
  @override
  void initState() {
    Provider.of<ArtistViewModel>(context, listen: false).getArtists();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Provider.of<UserViewModel>(context).isLogged
          ? FloatingActionButton(
              onPressed: () {
                Navigator.pushNamed(context, '/artist/add');
              },
              child: const Icon(Icons.add),
              backgroundColor: Colors.redAccent,
            )
          : null,
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Les artistes'),
      ),
      body: Consumer<ArtistViewModel>(
        builder: (context, vm, child) {
          return Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: ListView.separated(
                separatorBuilder: (context, int) {
                  return const SizedBox(height: 10);
                },
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(vm.artists[index].name),
                    onTap: () {
                      Navigator.pushNamed(context, '/artist', arguments: vm.artists[index]);
                    },
                  );
                },
                itemCount: vm.artists.length,
              ),
            ),
          );
        },
      ),
    );
  }
}
