import 'package:date_time_picker/date_time_picker.dart';
import 'package:festivalse/model/artist.dart';
import 'package:festivalse/view_model/artist.view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class NewArtistPage extends StatefulWidget {
  const NewArtistPage({Key? key}) : super(key: key);

  @override
  State<NewArtistPage> createState() => _NewArtistPageState();
}

class _NewArtistPageState extends State<NewArtistPage> {
  Artist artist = Artist(id: 0, name: '', passageStart: DateTime.now(), passageEnd: DateTime.now());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Nouvel artiste'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: TextField(
                  decoration: const InputDecoration(border: OutlineInputBorder(), hintText: 'Entrez le nom de l\'artiste', label: Text('Nom de l\'artiste')),
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    artist.name = value;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: DateTimePicker(
                  type: DateTimePickerType.dateTime,
                  firstDate: DateTime(2020),
                  initialDate: DateTime.now(),
                  onChanged: (value) {
                    artist.passageStart = DateTime.parse(value);
                  },
                  dateLabelText: 'Date et horaires de début',
                  lastDate: DateTime(2030),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: DateTimePicker(
                  type: DateTimePickerType.dateTime,
                  firstDate: DateTime(2020),
                  initialDate: DateTime.now(),
                  onChanged: (value) {
                    artist.passageEnd = DateTime.parse(value);
                  },
                  dateLabelText: 'Date et horaires de fin',
                  lastDate: DateTime(2030),
                ),
              ),
              ElevatedButton(
                onPressed: () => Provider.of<ArtistViewModel>(context, listen: false).addArtist(context, artist),
                child: const Text('Ajouter un artiste'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
