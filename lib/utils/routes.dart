import 'package:festivalse/view/artist.view.dart';
import 'package:festivalse/view/artist_new.view.dart';
import 'package:festivalse/view/artists.view.dart';
import 'package:festivalse/view/login.view.dart';
import 'package:festivalse/view/register.view.dart';
import 'package:flutter/material.dart';

Map<String, Widget Function(BuildContext)> routes = {
  '/login': (context) => const LoginPage(),
  '/register': (context) => const RegisterPage(),
  '/artists': (context) => const ArtistsPage(),
  '/artist': (context) => const ArtistPage(),
  '/artist/add': (context) => const NewArtistPage(),
};
