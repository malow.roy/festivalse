import 'dart:convert';

import 'package:festivalse/service/config.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class UserService {
  static void registerWithEmail(String email, String password) async {
    final response = await http.post(ConfigAPI.getUrl('/admin/register?email=${email}&password=${password}'), headers: ConfigAPI.defaultHeader);
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('RegisteredWithEmail');
      loginWithEmail(email, password);
    } else {
      print('Request failed with status: ${response.statusCode}.');
      print(response.body);
    }
  }

  static Future<bool> loginWithEmail(String email, String password) async {
    final response = await http.post(ConfigAPI.getUrl('/admin/authenticate?email=${email}&password=${password}'), headers: ConfigAPI.defaultHeader);

    print('login with email');
    print(response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      Map<String, dynamic> jsonContent = jsonDecode(response.body);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('user_access_token', '${jsonContent['accessToken']}');
      ConfigAPI.setToken(jsonContent['accessToken']);
      return true;
    } else {
      print('Request failed with status: ${response.statusCode}.');
      print(response.body);
      return false;
    }
  }
}
