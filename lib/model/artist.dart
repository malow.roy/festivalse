class Artist {
  int id;
  String name;
  DateTime passageStart;
  DateTime passageEnd;

  Artist({required this.id, required this.name, required this.passageStart, required this.passageEnd});

  factory Artist.fromJson(Map<String, dynamic> json) {
    return Artist(
      name: json['name'],
      id: json['id'],
      passageStart: json['startHour'] != null ? DateTime.parse(json['startHour']) : DateTime.now(),
      passageEnd: json['endHour'] != null ? DateTime.parse(json['endHour']) : DateTime.now(),
    );
  }
}
