import 'package:festivalse/model/user.dart';
import 'package:festivalse/service/user.service.dart';
import 'package:flutter/cupertino.dart';

class UserViewModel extends ChangeNotifier {
  User? user;
  bool isLogged = false;

  void loginWithEmail(BuildContext context, User user) async {
    isLogged = await UserService.loginWithEmail(user.email, user.password);
    notifyListeners();
    Navigator.pop(context);
  }

  void registerWithEmail(BuildContext context, User user, String confirmPassword) {
    if (confirmPassword == user.password) {
      UserService.registerWithEmail(user.email, user.password);
    } else {
      throw Error();
    }
    Navigator.pop(context);
  }

  void logout() {
    isLogged = false;
    user = null;
    notifyListeners();
  }
}
