import 'package:festivalse/view_model/user.view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void _openEndDrawer() {
    _scaffoldKey.currentState!.openEndDrawer();
  }

  _launchURL(String url) async {
    if (!await launch(url)) throw 'Could not launch $url';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      endDrawer: Drawer(
        child: SafeArea(
          child: Consumer<UserViewModel>(
            builder: (context, vm, child) {
              return ListView(
                children: [
                  DrawerHeader(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Column(
                        children: [
                          Icon(
                            vm.isLogged ? Icons.check : Icons.help_outline,
                            color: vm.isLogged ? Colors.green : Colors.black,
                            size: 80,
                          ),
                          Text(vm.isLogged ? "Vous êtes connecté(e)" : "Vous n'êtes pas connecté(e)")
                        ],
                      ),
                    ),
                  ),
                  if (!vm.isLogged)
                    ListTile(
                      title: const Text('Se connecter'),
                      onTap: () {
                        Navigator.pushNamed(context, '/login');
                      },
                    ),
                  if (!vm.isLogged)
                    ListTile(
                      title: const Text("S'inscrire"),
                      onTap: () {
                        Navigator.pushNamed(context, '/register');
                      },
                    ),
                  if (vm.isLogged)
                    ListTile(
                      title: const Text("Se déconnecter"),
                      onTap: () {
                        vm.logout();
                      },
                    )
                ],
              );
            },
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Image.network(
              'https://content.skyscnr.com/m/58008a95be7abe9d/original/eyeem-100010434-114562185.jpg',
              fit: BoxFit.cover,
            ),
            ListTile(
              title: const Text('Les artistes'),
              onTap: () {
                Navigator.pushNamed(context, '/artists');
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text(widget.title),
        actions: [
          IconButton(
            icon: const Icon(Icons.person),
            onPressed: () {
              _openEndDrawer();
            },
          )
        ],
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Image.network(
                'https://images.theconversation.com/files/278959/original/file-20190611-32373-xrhcrv.jpg?ixlib=rb-1.1.0&rect=1%2C0%2C997%2C666&q=45&auto=format&w=926&fit=clip'),
            const Padding(
              padding: EdgeInsets.only(top: 20),
              child: Center(
                child: Text(
                  'Le meilleur festival de france',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(top: 3),
              child: Center(
                child: Text(
                  'En plein milieu de Nantes, ce festival reccueille les meilleurs artistes dans un cadre de rêve. Electro, rap, pop, rock, il y en a pour tout les goûts !',
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Center(
                child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xff3b5998))),
                  onPressed: () {
                    _launchURL('https://www.univ-montp3.fr/miap/ens/info/Debutant/TD4/documents/test.html');
                  },
                  child: const Text('Facebook'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 3),
              child: Center(
                child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xffd93175))),
                  onPressed: () {
                    _launchURL('https://www.univ-montp3.fr/miap/ens/info/Debutant/TD4/documents/test.html');
                  },
                  child: const Text('Instagram'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 3),
              child: Center(
                child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xff3ab1db))),
                  onPressed: () {
                    _launchURL('https://www.univ-montp3.fr/miap/ens/info/Debutant/TD4/documents/test.html');
                  },
                  child: const Text('Twitter'),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 3),
              child: Center(
                child: ElevatedButton(
                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.redAccent)),
                  onPressed: () {
                    _launchURL('https://www.univ-montp3.fr/miap/ens/info/Debutant/TD4/documents/test.html');
                  },
                  child: const Text('Site web'),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
