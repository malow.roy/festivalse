import 'package:festivalse/model/user.dart';
import 'package:festivalse/view_model/user.view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  User user = User(email: '', password: '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: const Text('Connexion'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: TextField(
                  decoration: const InputDecoration(border: OutlineInputBorder(), hintText: 'Entrez votre email', label: Text('Email')),
                  keyboardType: TextInputType.emailAddress,
                  onChanged: (value) {
                    user.email = value;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: TextField(
                  decoration: const InputDecoration(border: OutlineInputBorder(), hintText: 'Entrez votre mot de passe', label: Text('Mot de passe')),
                  keyboardType: TextInputType.text,
                  obscureText: true,
                  onChanged: (value) {
                    user.password = value;
                  },
                ),
              ),
              ElevatedButton(
                onPressed: () => Provider.of<UserViewModel>(context, listen: false).loginWithEmail(context, user),
                child: Text('Se connecter'),
              )
            ],
          ),
        ),
      ),
    );
  }
}
