import 'package:festivalse/model/artist.dart';
import 'package:festivalse/view_model/artist.view_model.dart';
import 'package:festivalse/view_model/user.view_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class ArtistPage extends StatefulWidget {
  const ArtistPage({Key? key}) : super(key: key);

  @override
  State<ArtistPage> createState() => _ArtistPageState();
}

class _ArtistPageState extends State<ArtistPage> {
  @override
  Widget build(BuildContext context) {
    final artist = ModalRoute.of(context)!.settings.arguments as Artist;

    return Scaffold(
        floatingActionButton: Provider.of<UserViewModel>(context).isLogged
            ? FloatingActionButton(
                onPressed: () {
                  Provider.of<ArtistViewModel>(context, listen: false).removeArtist(context, artist);
                },
                child: const Icon(Icons.delete),
                backgroundColor: Colors.redAccent,
              )
            : null,
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(artist.name),
        ),
        body: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'Début du passage',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  artist.passageStart.toString(),
                  style: const TextStyle(fontSize: 15),
                ),
                const SizedBox(height: 30),
                const Text(
                  'Fin du passage',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  artist.passageEnd.toString(),
                  style: const TextStyle(fontSize: 15),
                ),
              ],
            ),
          ],
        ));
  }
}
