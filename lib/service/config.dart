abstract class ConfigAPI {
  static const String _apiUrl = 'http://192.168.1.20:8080/api';

  static Map<String, String> defaultHeader = {'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer'};
  static Map<String, String> headers = defaultHeader;

  static setToken(String? token) {
    headers = {'Content-type': 'application/json', 'Accept': 'application/json', 'Authorization': 'Bearer $token'};
  }

  static Uri getUrl(String apiCall) {
    return Uri.parse(_apiUrl + apiCall);
  }
}
