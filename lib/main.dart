import 'package:festivalse/utils/routes.dart';
import 'package:festivalse/view/home.view.dart';
import 'package:festivalse/view_model/artist.view_model.dart';
import 'package:festivalse/view_model/user.view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => UserViewModel()),
          ChangeNotifierProvider(create: (_) => ArtistViewModel()),
        ],
        builder: (context, child) {
          return MaterialApp(
            routes: routes,
            title: 'Festivalse',
            theme: ThemeData(
              primarySwatch: Colors.red,
            ),
            home: const MyHomePage(title: 'Festivalse'),
          );
        });
  }
}
