import 'dart:convert';

import 'package:festivalse/model/artist.dart';
import 'package:festivalse/service/config.dart';
import 'package:http/http.dart' as http;

class ArtistService {
  static Future<List<Artist>?> getArtists() async {
    List<Artist> artists = [];
    final response = await http.get(ConfigAPI.getUrl('/artist'), headers: ConfigAPI.headers);
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('artists get');
      final List<dynamic> jsonResponse = jsonDecode(response.body);
      for (int i = 0; i < jsonResponse.length; i++) {
        artists.add(Artist.fromJson(jsonResponse[i]));
      }
      return artists;
    } else {
      print('Request failed with status: ${response.statusCode}.');
      print(response.body);
    }
  }

  static Future<String?> addArtist(String name, DateTime passageStart, DateTime passageEnd) async {
    print('addArtist');
    print(ConfigAPI.headers);
    final response = await http.post(
      ConfigAPI.getUrl('/artist'),
      headers: ConfigAPI.headers,
      body: jsonEncode(
        <String, String>{"name": name, "startHour": passageStart.toString(), "endHour": passageEnd.toString()},
      ),
    );
    print(response.body);
    if (response.statusCode == 401) {
      return "Unauthorized";
    }
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('artists added');
      return "Ok";
    } else {
      print('Request failed with status: ${response.statusCode}.');
      print(response.body);
    }
  }

  static Future<String?> removeArtist(int id) async {
    final response = await http.delete(ConfigAPI.getUrl('/artist/$id'), headers: ConfigAPI.headers);
    if (response.statusCode == 401) {
      return "Unauthorized";
    }
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('artists removed');
      return "Ok";
    } else {
      print('Request failed with status: ${response.statusCode}.');
      print(response.body);
    }
  }
}
